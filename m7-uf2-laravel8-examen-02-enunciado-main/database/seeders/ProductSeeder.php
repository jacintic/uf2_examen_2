<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Product::factory(4)->create();
        DB::table('products')->insert(
            [
                [
                    'name' => "El señor de los anillos",
                    'category' => 1,
                    'description' => 'Algo de un anillo',
                    'rating' => 4,
                    'stock' => 7,
                    'price' => 10.10,
                    'image' => 'esdla.jpg',
                ],
                [
                    'name' => "Ironman",
                    'category' => 2,
                    'description' => 'Un tio con mucha pasta',
                    'rating' => 4,
                    'stock' => 1,
                    'price' => 5.0,
                    'image' => 'im.jpg'
                ],
                [
                    'name' => "Passengers",
                    'category' => 3,
                    'description' => 'Una nave espacial',
                    'rating' => 2,
                    'stock' => 3,
                    'price' => 1.0,
                    'image' => 'p.jpg'
                ],
                [
                    'name' => "V de Vendetta",
                    'category' => 2,
                    'description' => 'Venganza',
                    'rating' => 2,
                    'stock' => 3,
                    'price' => 1.00,
                    'image' => 'vdv.jpg'
                ]
                ]);

    }
}
