<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|between:8,12',
            'email' => 'required|between:8,15',
            'address' => 'required|between:10,18',
            'pass' => 'required|between:8,12|confirmed',
            'pass_confirmation' => 'required|between:8,12',
            'photo' => 'required',
        ];

    }
    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
   public function messages()
   {
    return [
        'between' => 'ERROR: El campo :attribute deve ser entre :min y :max de largo.',
        'required' => 'ERROR: Falta el campo :attribute .',
        'confirmed' => 'EL campo :attribute deve coincidir con el campo de confirmación de password.'
    ];
   }
}
